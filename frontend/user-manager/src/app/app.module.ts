import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UsersListComponent } from './components/users-list/users-list.component';
import { NewUserFormComponent } from './components/new-user-form/new-user-form.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { UsersService } from './services/users.service';
import { UserAccountComponent } from './components/user-account/user-account.component';
import { UpdateUserFormComponent } from './components/update-user-form/update-user-form.component';
import { UsersGridComponent } from './components/users-grid/users-grid.component';
@NgModule({
  declarations: [
    AppComponent,
    UsersListComponent,
    NewUserFormComponent,
    UserAccountComponent,
    UpdateUserFormComponent,
    UsersGridComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
