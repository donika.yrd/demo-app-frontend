import { NgModule } from '@angular/core';
import { Routes, RouterModule, GuardsCheckStart } from '@angular/router';
import { UsersListComponent } from './components/users-list/users-list.component';
import { NewUserFormComponent } from './components/new-user-form/new-user-form.component';
import { UserAccountComponent } from './components/user-account/user-account.component';
import { UpdateUserFormComponent } from './components/update-user-form/update-user-form.component';
import { UsersGridComponent } from './components/users-grid/users-grid.component';


const routes: Routes = [
  {path: 'users', component: UsersListComponent}, 
  {path: 'users/grid', component: UsersGridComponent},
  {path: 'users/new', component: NewUserFormComponent},
  {path: 'users/:id', component: UserAccountComponent},
  {path: 'users/:id/update', component: UpdateUserFormComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
